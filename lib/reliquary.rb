require_relative "reliquary/version"
require "rest-client"

module Reliquary
  class NewRelicAPIHandler

    def initialize(apikey)
      @apikey = apikey
    end

    def getNewRelicData(name, appid)
      appData = JSON.parse(RestClient.get("https://api.newrelic.com/v2/applications/#{appid}.json",
        :params => {"names[]" => 'HttpDispatcher', "values[]" => 'requests_per_minute'},
        :content_type => :json, "X-Api-Key" => "#{@apikey}"))
      appData["application"]
    end

    def getNewRelicAppId(filter)
      apps = Hash.new
      appList = JSON.parse(RestClient.get("https://api.newrelic.com/v2/applications.json",
        :content_type => :json, "X-Api-Key" => "#{@apikey}"))
      appList['applications'].select { |app| app['name'].include?(filter) && app['reporting'] }.each do |app|
        apps[app["name"]] = app["id"]
      end
      apps
    end

  end

end
