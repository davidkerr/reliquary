require 'pg'

module Reliquary

  class Postgres
    def initialize(host = 'localhost')
      @postgres = PG.connect( dbname: 'reliquary', host: host )
    end

    def writeData(name, data)
      @postgres.exec( "insert into app_metrics(name, timestamp, metrics)
                           VALUES ('#{name}', '#{data['last_reported_at']}', 
                           '#{data.to_json}')" )
    end

  end
end
