require 'influxdb'
module Reliquary
  class Influx

    def initialize(host = 'localhost')
        @influx = InfluxDB::Client.new('reliquary',host: host)
    end

    def writeData(name, data)
      d = { values: data['application_summary'] }
      @influx.write_point(name, d)
    end
  end
end
